import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.css']
})

export class ClockComponent implements OnInit {

  constructor() { }

  updateClock() {
    const currentTime = new Date();

    let currentHours = currentTime.getHours();
    const currentMinutes = currentTime.getMinutes();
    const currentSeconds = currentTime.getSeconds();

    const minutesString = (currentMinutes < 10 ? '0' : '') + currentMinutes;
    const secondString = (currentSeconds < 10 ? '0' : '') + currentSeconds;

    const timeOfDay = (currentHours < 12) ? 'AM' : 'PM';

    currentHours = (currentHours > 12) ? currentHours - 12 : currentHours;

    currentHours = (currentHours === 0) ? 12 : currentHours;

    document.getElementById('clock').innerText = currentHours + ':' + minutesString + ':' + secondString + ' ' + timeOfDay;
  }

  ngOnInit() {
    this.updateClock();
    setInterval(this.updateClock, 11000 );
  }

}
