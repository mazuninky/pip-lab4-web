import {Component, OnInit, ViewChild} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {PlotComponent} from '../plot/plot.component';
import {HitResult} from '../data-model';
import axios from 'axios';
import {Router} from '@angular/router';
import {default as BigNumber} from 'bignumber.js';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})


export class EditorComponent implements OnInit {
  pointForm: FormGroup;

  @ViewChild(PlotComponent) plot: PlotComponent;

  hitResults: HitResult[];

  constructor(private router: Router) {
    this.hitResults = [];
  }

  sendPoint(x, y) {
    if (parseFloat(this.pointForm.value.r) > 0) {
      const hitResults = this.hitResults;
      const plot = this.plot;
      axios.post('http://localhost:6533/point', `x=${x}&y=${y}&r=${this.pointForm.value.r}`, {
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        withCredentials: true
      }).then(function (response) {
        hitResults.push(new HitResult(response.data.x, response.data.y, response.data.r, response.data.inArea));
        plot.drawPoint(parseFloat(x), parseFloat(y), response.data.inArea);
      }).catch(function (error) {
        console.log(error);
      });
    }
  }

  onLogout() {
    const context = this;
    axios.post('http://localhost:6533/logout', '', {
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      withCredentials: true
    }).then(function (response) {
      context.router.navigate(['/welcome']);
    }).catch(function (error) {
      console.log(error);
    });
  }

  refreshTable() {
    const context = this;
    axios.get('http://localhost:6533/points', {
      withCredentials: true
    }).then(function (response) {
      context.hitResults = response.data;
      context.invalidatePoints(context.pointForm.value.r);
    }).catch(function (error) {
      console.log(error);
    });
  }

  invalidatePoints(r) {
    for (const result of this.hitResults) {
      if (result.r == r) {
        this.plot.drawPoint(parseFloat(result.x), parseFloat(result.y), result.inArea);
      }
    }
  }

  onSubmit() {
    this.sendPoint(this.pointForm.value.x, this.pointForm.value.y);
  }

  xValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value.match('^[+-]?\\d+(\\.\\d+)?')) {
        return {'not a number': {value: control.value}};
      } else {
        const value = new BigNumber(control.value);
        const out = value.cmp(3) >= 0 || value.cmp(-5) <= 0;
        return out ? {'out of range': {value: control.value}} : null;
      }
    };
  }

  ngOnInit() {
    this.pointForm = new FormGroup({
      x: new FormControl('0'),
      y: new FormControl('0', [Validators.required, this.xValidator()]),
      r: new FormControl('1', [Validators.min(1)])
    });

    this.pointForm.get('r').valueChanges.subscribe(val => {
      if (val > 0) {
        this.plot.drawPlot(Number(val));
        this.invalidatePoints(val);
      }
    });

    this.plot.injectParentComponent(this);

    this.refreshTable();
  }

}
