import {Component, OnInit} from '@angular/core';
import {EditorComponent} from '../editor/editor.component';

@Component({
  selector: 'app-plot',
  templateUrl: './plot.component.html',
  styleUrls: ['./plot.component.css']
})

export class PlotComponent implements OnInit {
  private parent: EditorComponent;

  injectParentComponent(component: EditorComponent) {
    this.parent = component;
  }

  constructor() {
  }

  drawPlot(rValue: number) {
    const pointPerValue = 30;
    const plot: any = document.getElementById('plot');
    const plotWidth = plot.width;
    const plotHeight = plot.height;
    const xCenter: number = plotWidth / 2;
    const yCenter = plotHeight / 2;
    const context = plot.getContext('2d');

    context.fillStyle = '#FFFFFF';
    context.fillRect(0, 0, plotWidth, plotHeight);

    const r = pointPerValue * rValue;

    context.beginPath();
    context.strokeStyle = '#000000';
    context.moveTo(xCenter, 0);
    context.lineTo(xCenter, plotHeight);
    context.stroke();
    context.closePath();

    context.beginPath();
    context.moveTo(0, yCenter);
    context.lineTo(plotWidth, yCenter);
    context.stroke();
    context.closePath();

    context.fillStyle = '#3399FF';

    context.beginPath();
    context.fillRect(xCenter + 1, yCenter - 1, r / 2 + 1, -r - 1);
    context.closePath();

    context.beginPath();
    context.moveTo(xCenter - 1, yCenter + 1);
    context.lineTo(xCenter - 1 - r, yCenter + 1);
    context.lineTo(xCenter - 1, yCenter + 1 + r / 2);
    context.moveTo(xCenter - 1, yCenter + 1);
    context.fill();
    context.closePath();

    context.beginPath();
    context.arc(xCenter + 1, yCenter + 1, r, Math.PI / 2, 0, true);
    context.moveTo(xCenter + 1 + r, yCenter + 1);
    context.lineTo(xCenter + 1, yCenter + 1);
    context.lineTo(xCenter + 1, yCenter + 1 + r);
    context.fill();
    context.closePath();
  }

  drawPoint(x, y, hit) {
    const pointPerValue = 30;
    const plot: any = document.getElementById('plot');
    const plotWidth = plot.width;
    const plotHeight = plot.height;
    const xCenter: number = plotWidth / 2;
    const yCenter = plotHeight / 2;
    const context = plot.getContext('2d');

    context.beginPath();
    if (hit) {
      context.fillStyle = '#00FF00';
    } else {
      context.fillStyle = '#FF0000';
    }
    y = -y;
    context.arc(x * pointPerValue + xCenter - 2, y * pointPerValue + yCenter - 2, 4, 0, 2 * Math.PI);
    context.fill();
  }

  ngOnInit() {
    this.drawPlot(1);
    const plot: any = document.getElementById('plot');
    const parent = this.parent;
    plot.addEventListener('click', function (event) {
      const pointPerValue = 30;
      const plotWidth = plot.width;
      const plotHeight = plot.height;
      const xCenter: number = plotWidth / 2;
      const yCenter = plotHeight / 2;
      const context = plot.getContext('2d');
      const x = event.pageX - plot.offsetLeft - xCenter,
        y = -event.pageY + plot.offsetTop + yCenter;
      parent.sendPoint(x / pointPerValue, y / pointPerValue);
    }, false);
  }
}
